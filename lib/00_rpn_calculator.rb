class RPNCalculator
  def initialize
    @num_arr = []
  end

  def push(num)
    @num_arr << num
  end

  def value
    @num_arr.last
  end

  def plus
    raise "calculator is empty" if @num_arr.size < 2
    @num_arr << @num_arr.pop + @num_arr.pop
  end

  def minus
    raise "calculator is empty" if @num_arr.size < 2
    @y = @num_arr.pop
    @x = @num_arr.pop
    @num_arr << @x - @y
  end

  def divide
    raise "calculator is empty" if @num_arr.size < 2
    @y = @num_arr.pop
    @x = @num_arr.pop
    @num_arr << @x.fdiv(@y)
  end

  def times
    raise "calculator is empty" if @num_arr.size < 2
    @num_arr << @num_arr.pop * @num_arr.pop
  end

  def tokens(arg)
    nums = ("1".."10").to_a
    token_arr = arg.split
    token_arr = token_arr.map { |ele| nums.include?(ele) ? ele.to_i : ele.to_sym}
  end

  def evaluate(arg)
    token_arr = tokens(arg)
    token_arr.each do |tok|
      case tok
      when Integer
        push(tok)
      else
        if tok == :+
          plus
        elsif tok == :-
          minus
        elsif tok == :/
          divide
        else
          times
        end
      end
    end
    value
  end
end
